<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Validator;


class UserController extends BaseController
{

    protected $storageImagePath;
    public function __construct()
    {
        $this->storageImagePath = url('/').'/storage/app/';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Users = User::all();
        return $this->sendResponse($Users->toArray(), 'Users retrieved successfully.');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'address',
            'zip' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input['profile_pic']=$this->uploadImage($request->profile_pic);
        $input['document_img']=$this->uploadImage($request->document_img);
        $User = User::create($input);
        return $this->sendResponse($User->toArray(), 'User created successfully.');
    }

    public function uploadImage($file){
          $file = $file;
          $path = $file->path();
          $extension = $file->extension();
          $pathUploadImage=$file->store('images');
          return $this->storageImagePath.$pathUploadImage;
    }



}

 ?>
